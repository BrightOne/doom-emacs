;; -*- no-byte-compile: t; -*-
;;; lang/markdown/packages.el

(package! markdown-mode)
(package! markdown-toc)
(package! edit-indirect)

(when (featurep! +pandoc)
  (package! pandoc-mode))
